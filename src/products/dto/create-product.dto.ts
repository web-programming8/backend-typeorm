import { IsNotEmpty, Length, IsPositive } from 'class-validator';

export class CreateProductDto {
  @Length(4, 16)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
